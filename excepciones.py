class NotFoundException(Exception):
    pass

class BadRequestException(Exception):
    pass

class OrdenNotFoundException(Exception):
    pass

class TransporteNotFoundException(Exception):
    pass

class VolumenTransporteLimitException(Exception):
    pass

class EnvioExistsException(Exception):
    pass

class FechaTransporteNotAvailableException(Exception):
    pass