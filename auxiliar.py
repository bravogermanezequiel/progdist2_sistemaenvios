import json
from datetime import date, timedelta, datetime
from config_uri import URL_LIBRERIA, URL_ORDENES
from lib.RequestWrapper import RequestWrapper

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

class BinPacking:
    @staticmethod
    def pack(c, weight=[]):     
        n = len(weight)
        # Initialize result (Count of bins)
        res = 0
        # Create an array to store remaining space in bins
        # there can be at most n bins
        bin_rem = [0]*n
        # Place items one by one
        for i in range(n):
            # Find the first bin that can accommodate
            # weight[i]
            j = 0
            while( j < res):
                if (bin_rem[j] >= weight[i]):
                    bin_rem[j] = bin_rem[j] - weight[i]
                    break
                j+=1
            # If no bin could accommodate weight[i]
            if (j == res):
                bin_rem[res] = c - weight[i]
                res= res+1
        return res
    
    @staticmethod
    def packAll(c=[], weight=[]):
        if not len(c) or not len(weight):
            return {}

        capacities = sorted(c, reverse=True)
        bins = { c: 0 for c in capacities }
        bins[capacities[-1]] = BinPacking.pack(capacities[-1], weight)
        if len(c) == 1:
            return bins

        for c in capacities[:-1]:
            if ((bins[capacities[-1]]*capacities[-1]) // c) > 0:
                bins[c] = (bins[capacities[-1]]*capacities[-1]) // c
                bins[capacities[-1]] -= c//capacities[-1]*bins[c]
        return bins

class ConsultorLibros:
    @staticmethod
    def obtenerVolumenLibro(id):
        URL = f"{URL_LIBRERIA}/libro/{id}"
        r = RequestWrapper.get(URL)
        
        if r.status_code != HTTP_OK_REQUEST:
            return None
        
        data = json.loads(r.text)

        return data.get("volumen")

class ConsultorOrdenes:
    @staticmethod
    def getOrden(id):
        URL = f"{URL_ORDENES}/orden/{id}"
        r = RequestWrapper.get(URL)
        
        if r.status_code != HTTP_OK_REQUEST:
            return None

        data = json.loads(r.text)

        return data

class ConsultorTransportes:
    @staticmethod
    def dateForWeekday(day, weekFromHere=1):
        today = date.today()
        # weekday returns the offsets 0 - 6
        # If you need 1 - 7, use isoweekday
        weekday = today.weekday()
        d = today + timedelta(days = day - weekday + (7*(weekFromHere-1)))

        return d.strftime("%d-%m-%Y")
        
    def isPastDate(dateStr):
        today = datetime.today()
        return today > datetime.strptime(dateStr, "%d-%m-%Y")