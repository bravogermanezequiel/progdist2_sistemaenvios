from flask import jsonify
from entidades import *
from app import db
from excepciones import *
import datetime
from auxiliar import BinPacking, ConsultorLibros, ConsultorOrdenes, ConsultorTransportes
import itertools

class RootRepositorio:
    def get(self):
        return { "Servidor": "Servicio Envios" }
        
class TransportesRepositorio:
    def list(self):
        transportes = Transporte.query.order_by(Transporte.patente).all()
    
        data = {
            "transportes": [
                {
                    "patente": x.patente, 
                    "volumen": x.volumen, 
                    "disponibilidadesHorarias": [
                        [
                            disponibilidadHoraria.diaSemanal,
                            disponibilidadHoraria.horarioInicial,
                            disponibilidadHoraria.horarioFinal
                        ] for disponibilidadHoraria in x.disponibilidadesHorarias
                    ]
                } for x in transportes
            ]
        }

        return data
    
    def get(self, patente):
        transporte = Transporte.query.get(patente)
    
        if transporte is None:
            raise NotFoundException
            
        return {
            "patente": transporte.patente,
            "volumen": transporte.volumen,
            "disponibilidadesHorarias": [
                (
                    disponibilidadHoraria.diaSemanal,
                    disponibilidadHoraria.horarioInicial,
                    disponibilidadHoraria.horarioFinal
                ) for disponibilidadHoraria in transporte.disponibilidadesHorarias
            ]
        }
    
    def create(self):
        from flask import request
        json = request.get_json()
        patente = json.get("patente")
        volumen = json.get("volumen")
        disponibilidadesHorarias = json.get("disponibilidadesHorarias")
        print(disponibilidadesHorarias, flush=True)

        for i, (diaSemanal, horarioInicial, horarioFinal) in enumerate(disponibilidadesHorarias):
            if not isinstance(diaSemanal, int):
                raise BadRequestException(f"El dia semanal {diaSemanal} no es un dato aceptable (solo aceptan enteros)")
            elif diaSemanal not in range(0,7):
                raise BadRequestException(f"El dia semanal {diaSemanal} debe estar entre los valores 0..6")

            _ = datetime.datetime.strptime(horarioInicial, "%H:%M")
            _ = datetime.datetime.strptime(horarioFinal, "%H:%M")
            disponibilidadesHorarias[i] = DisponibilidadTransporte(patente, diaSemanal, horarioInicial, horarioFinal)

        newTransporte = Transporte(patente, volumen, disponibilidadesHorarias)

        db.session.add(newTransporte)
        db.session.commit()

        return {
            "id": newTransporte.patente
        }

    def update(self, patente):
        transporte = Transporte.query.get(patente)
    
        if transporte is None:
            raise NotFoundException
        
        columnas = transporte.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso

        from flask import request
        js = request.get_json()
        
        if "disponibilidadesHorarias" in columnas:
            columnas.remove("disponibilidadesHorarias")
        
        for columna in columnas:
            setattr(transporte, columna, js.get(columna))
        
        db.session.add(transporte)
        db.session.commit()

        return {
            "patente": transporte.patente
        }

    def listTransportesConFechas(self, week):
        transportes = self.list().get("transportes")

        if transportes is None:
            raise NotFoundException
        
        for i, transporte in enumerate(transportes):
            disponibilidad = []
            for j, disp in enumerate(transporte["disponibilidadesHorarias"]):
                dateStr = ConsultorTransportes.dateForWeekday(disp[0], week)
                if not ConsultorTransportes.isPastDate(dateStr):
                    disponibilidad += [transportes[i]["disponibilidadesHorarias"][j] + [dateStr]]

            transportes[i]["disponibilidadesHorarias"] = disponibilidad
        
        return transportes

    def listTransportesConFechasVolumen(self, volume, week=1):
        transportes = self.listTransportesConFechas(week)

        if transportes is None:
            raise NotFoundException
        
        return list(filter(lambda tr: tr["volumen"] >= volume, transportes))

class EnviosRepositorio:
    def process(self, ordenId):
        orden = ConsultorOrdenes.getOrden(ordenId)
        if orden is None:
            raise NotFoundException

        librosCantidad = orden.get("librosCantidad")
        librosIds = set(map(lambda x: x[0], librosCantidad))
        volLibros = { libroId: ConsultorLibros.obtenerVolumenLibro(libroId) for libroId in librosIds }
        preItems = [
            [volLibros[libroId]]*cantidad
            for [libroId, cantidad] in librosCantidad
        ]
        items = list(itertools.chain(*preItems))
        boxesNeeded = BinPacking.packAll([40,4], items)
        volTotal = sum([ k*v for k,v in boxesNeeded.items() ])
        return {
            "boxesNeeded" : boxesNeeded,
            "volTotal": volTotal
        }
    
    def list(self):
        envios = Envio.query.order_by(Envio.id).all()
    
        data = {
            "envios": [
                {
                    "id": x.id, 
                    "patente": x.patente, 
                    "fecha": x.fecha,
                    "ordenes": [ 
                        orden.ordenId for orden in x.ordenesEnvio
                    ]
                } for x in envios
            ]
        }

        return data
    
    def get(self, id):
        envio = Envio.query.get(id)
    
        if envio is None:
            raise NotFoundException
            
        return {
            "id": envio.id,
            "patente": envio.patente,
            "fecha": envio.fecha,
            "ordenes": [
                orden.ordenId for orden in envio.ordenesEnvio
            ]
        }
    
    def filter(self, patente, fecha):
        envio = Envio.query.filter_by(patente=patente).filter_by(fecha=fecha).first()

        if envio is None:
            raise NotFoundException
        
        return {
            "id": envio.id,
            "patente": envio.patente,
            "fecha": envio.fecha,
            "ordenes": [
                orden.ordenId for orden in envio.ordenesEnvio
            ]
        }

    def create(self):
        from flask import request
        json = request.get_json()
        patente = json.get("patente")
        fecha = json.get("fecha")
        ordenes = json.get("ordenes")

        fechaDate = datetime.datetime.strptime(fecha, "%d-%m-%Y")

        try:
            self.filter(patente, fecha)
            raise EnvioExistsException
        except NotFoundException:
            pass

        try:
            transporte = TransportesRepositorio().get(patente)
        except NotFoundException:
            raise TransporteNotFoundException

        volumenTotal = 0
        for i, ordenId in enumerate(ordenes):
            if ConsultorOrdenes.getOrden(ordenId) is None:
                raise OrdenNotFoundException(str(ordenId))
            
            volumenTotal += self.process(ordenId).get("volTotal")
            ordenes[i] = OrdenesPorEnvio(ordenId)

        if volumenTotal > transporte.get("volumen"):
            raise VolumenTransporteLimitException

        weekDays = list(map(lambda d: d[0], transporte["disponibilidadesHorarias"]))
        if not fechaDate.weekday() in weekDays:
            raise FechaTransporteNotAvailableException

        newEnvio = Envio(patente, fecha, ordenes)

        db.session.add(newEnvio)
        db.session.commit()

        return {
            "id": newEnvio.id
        }

    def update(self, id):
        envio = Envio.query.get(id)

        if envio is None:
            raise NotFoundException
            
        columnas = envio.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso

        from flask import request
        js = request.get_json()

        for columna in columnas:
            setattr(envio, columna, js.get(columna))        
        
        patente = js.get("patente")
        fecha = js.get("fecha")
        ordenes = js.get("ordenes")

        fechaDate = datetime.datetime.strptime(fecha, "%d-%m-%Y")

        try:
            transporte = TransportesRepositorio().get(patente)
        except NotFoundException:
            raise TransporteNotFoundException

        volumenTotal = 0
        envio.ordenesEnvio = []

        for i, ordenId in enumerate(ordenes):
            if ConsultorOrdenes.getOrden(ordenId) is None:
                raise OrdenNotFoundException(str(ordenId))
            
            volumenTotal += self.process(ordenId).get("volTotal")
            envio.ordenesEnvio += [OrdenesPorEnvio(ordenId)]

        if volumenTotal > transporte.get("volumen"):
            raise VolumenTransporteLimitException

        weekDays = list(map(lambda d: d[0], transporte["disponibilidadesHorarias"]))
        if not fechaDate.weekday() in weekDays:
            raise FechaTransporteNotAvailableException
            
        db.session.add(envio)
        db.session.commit()

        return {
            "id": envio.id
        }