from flask import request, Blueprint
from repositorio import *
from excepciones import *
import logging

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

ruta = Blueprint("rutas", __name__, url_prefix="")

@ruta.route("/", methods=["GET"])
def server_info():
    return jsonify(RootRepositorio().get()), HTTP_OK_REQUEST

@ruta.route("/transportes/", methods=["GET"])
def listTransportes():
    return jsonify(TransportesRepositorio().list()), HTTP_OK_REQUEST 

@ruta.route("/transporte/<string:patente>", methods=["GET"])
def getTransporte(patente):
    try:
        transporte = TransportesRepositorio().get(patente)
        return jsonify(transporte), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/transporte/", methods=["POST"])
def createTransporte():
    try:
        resp = TransportesRepositorio().create()
        
        return jsonify(resp), HTTP_OK_CREATED
    except (BadRequestException, ValueError) as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/transporte/<string:patente>", methods=["PUT"])
def updateTransporte(patente):
    try:
        resp = TransportesRepositorio().update(patente)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": "No encontrado" }), HTTP_NOT_FOUND
    except (BadRequestException, ValueError) as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/transportes/dates/<int:week>", methods=["GET"])
def listTransportesConFechas(week):
    return jsonify(TransportesRepositorio().listTransportesConFechas(week)), HTTP_OK_REQUEST 

@ruta.route("/envio/process/<int:ordenId>", methods=["GET"])
def processEnvio(ordenId):
    try:
        resp = EnviosRepositorio().process(ordenId)
        return jsonify(resp), HTTP_OK_REQUEST
    except NotFoundException:
        return jsonify({ "Resultado": f"Orden con ordenId = {ordenId} no encontrada" }), HTTP_NOT_FOUND
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/envios/", methods=["GET"])
def listEnvios():
    return jsonify(EnviosRepositorio().list()), HTTP_OK_REQUEST 

@ruta.route("/envio/<int:id>", methods=["GET"])
def getEnvio(id):
    try:
        envio = EnviosRepositorio().get(id)
        return jsonify(envio), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/envio/<string:patente>/<string:fecha>", methods=["GET"])
def filterEnvio(patente, fecha):
    try:
        envio = EnviosRepositorio().filter(patente, fecha)
        return jsonify(envio), HTTP_OK_REQUEST
    except NotFoundException as e:
        return jsonify({ "Resultado": "No encontrado"}), HTTP_NOT_FOUND

@ruta.route("/envio/", methods=["POST"])
def createEnvio():
    try:
        resp = EnviosRepositorio().create()
        return jsonify(resp), HTTP_OK_CREATED
    except TransporteNotFoundException:
        return jsonify({ "Resultado": "La patente del vehiculo no existe"}), HTTP_BAD_REQUEST
    except OrdenNotFoundException as e:
        return jsonify({ "Resultado": f"La orden con id {str(e)} no existe"}), HTTP_BAD_REQUEST
    except VolumenTransporteLimitException:
        return jsonify({ "Resultado": "El volumen necesario para realizar el envio sobrepasa el limite del vehiculo" }), HTTP_BAD_REQUEST
    except EnvioExistsException:
        return jsonify({ "Resultado": "Ya existe un envio para esa fecha para el vehiculo solicitado"}), HTTP_BAD_REQUEST
    except FechaTransporteNotAvailableException:
        return jsonify({ "Resultado": "La fecha no se encuentra entre los dias disponibles para el transporte elegido"}), HTTP_BAD_REQUEST
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST

@ruta.route("/envio/<int:id>", methods=["PUT"])
def update(id):
    try:
        resp = EnviosRepositorio().update(id)
        return jsonify(resp), HTTP_OK_REQUEST
    except TransporteNotFoundException:
        return jsonify({ "Resultado": "La patente del vehiculo no existe"}), HTTP_BAD_REQUEST
    except OrdenNotFoundException as e:
        return jsonify({ "Resultado": f"La orden con id {str(e)} no existe"}), HTTP_BAD_REQUEST
    except VolumenTransporteLimitException:
        return jsonify({ "Resultado": "El volumen necesario para realizar el envio sobrepasa el limite del vehiculo" }), HTTP_BAD_REQUEST
    except FechaTransporteNotAvailableException:
        return jsonify({ "Resultado": "La fecha no se encuentra entre los dias disponibles para el transporte elegido"}), HTTP_BAD_REQUEST
    except BadRequestException as e:
        return jsonify({ "Resultado": str(e) }), HTTP_BAD_REQUEST



@ruta.before_request
def beforeRequest():
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    body = request.data.replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Request. request.method={method} request.endpoint={endpoint} request.body='{body}'")

@ruta.after_request
def afterRequest(response):
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    data = response.get_data().replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Response. response.method={method} response.endpoint={endpoint} response.data='{data}' response.status_code={response.status_code}")
    return response