from app import db
from sqlalchemy.orm import relationship

class Transporte(db.Model):
    __tablename__ = "transportes"
    patente = db.Column(db.String(7), primary_key=True)
    volumen = db.Column(db.Integer)
    disponibilidadesHorarias = relationship("DisponibilidadTransporte")

    def __init__(self, patente, volumen, disponibilidadesHorarias):
        self.patente = patente
        self.volumen = volumen
        self.disponibilidadesHorarias = disponibilidadesHorarias

    def toDict(self):
        obj = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        obj["disponibilidadesHorarias"] = [ disponibilidadHoraria.toDict() for disponibilidadHoraria in getattr(self, "disponibilidadesHorarias") ]
        return obj

class DisponibilidadTransporte(db.Model):
    __tablename__ = "disponibilidadesTransportes"
    id = db.Column(db.Integer, primary_key=True)
    patente = db.Column(db.String(7), db.ForeignKey('transportes.patente'))
    diaSemanal = db.Column(db.Integer)
    horarioInicial = db.Column(db.String(64))
    horarioFinal = db.Column(db.String(64))       

    def __init__(self, patente, diaSemanal, horarioInicial, horarioFinal):
        self.patente = patente
        self.diaSemanal = diaSemanal
        self.horarioInicial = horarioInicial
        self.horarioFinal = horarioFinal
    
    def toDict(self):
       return [getattr(self, "diaSemanal"), getattr(self, "horarioInicial"), getattr(self, "horarioFinal")]

class Envio(db.Model):
    __tablename__ = "envios"
    id = db.Column(db.Integer, primary_key=True)
    patente = db.Column(db.String(7), db.ForeignKey('transportes.patente'))
    fecha = db.Column(db.String(64))
    ordenesEnvio = relationship("OrdenesPorEnvio")

    def __init__(self, patente, fecha, ordenesEnvio):
        self.patente = patente
        self.fecha = fecha
        self.ordenesEnvio = ordenesEnvio
        
    def toDict(self):
        obj = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        obj["ordenesEnvio"] = [ ordenEnvio.toDict() for ordenEnvio in getattr(self, "ordenesEnvio") ]
        return obj

class OrdenesPorEnvio(db.Model):
    __tablename__ = "ordenesPorEnvio"
    id = db.Column(db.Integer, primary_key=True)
    envioId = db.Column(db.Integer, db.ForeignKey('envios.id'))
    ordenId = db.Column(db.Integer)

    def __init__(self, ordenId):
        self.ordenId = ordenId

    def toDict(self):
       return getattr(self, "ordenId")
