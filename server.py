from app import initApp, db
import os
from config_log import CONFIG_LOGGING
import logging
import logging.config

logging.config.dictConfig(CONFIG_LOGGING)
app = initApp()

if __name__ == '__main__':
    with app.app_context():
        db.create_all()

    app.run(host="0.0.0.0", port=3000)