import unittest
from app import initApp, db

class BaseTestClass(unittest.TestCase):
    def setUp(self):
        self.app = initApp("ConfigTest")
        self.client = self.app.test_client()
        with self.app.app_context():
            # Creamos todas las tablas de la base de datos
            # Si ya existen las tablas, no se crearan nuevamente...
            db.create_all() 

    def tearDown(self):
        with self.app.app_context():
            # Elimina todas las tablas de la base de datos
            db.session.commit()
            db.session.remove()
            db.drop_all()