from . import BaseTestClass
from app import db
from entidades import Transporte, DisponibilidadTransporte, Envio, OrdenesPorEnvio
from auxiliar import ConsultorOrdenes, ConsultorLibros
from repositorio import TransportesRepositorio, EnviosRepositorio
import json
from mock import patch
from excepciones import *

class TestTransporte(BaseTestClass):
    def setUp(self):
        super().setUp()

        with self.app.app_context():
            transportesData = []
            transportesData += [
                {
                    "patente": "AB123CD", 
                    "volumen": 500,
                    "disponibilidadesHorarias": [
                        [ 0, "05:00", "18:00" ],
                        [ 1, "06:00", "19:00" ],
                        [ 2, "07:00", "20:00" ]
                    ]
                }
            ]
            objTransporte = transportesData[0].copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transportesData[0].get("patente"), *disp) for disp in transportesData[0]["disponibilidadesHorarias"] 
            ]
            transporte1 = Transporte(**objTransporte)
            db.session.add(transporte1)
            
            transportesData += [
                {
                    "patente": "EF456GH", 
                    "volumen": 320,
                    "disponibilidadesHorarias": [
                        [ 0, "09:00", "18:00" ],
                        [ 1, "09:00", "18:00" ],
                        [ 2, "09:00", "17:00" ],
                        [ 3, "09:00", "16:00" ],
                        [ 4, "09:00", "15:30" ]
                    ]
                }
            ]
            objTransporte = transportesData[1].copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transportesData[1].get("patente"), *disp) for disp in transportesData[1]["disponibilidadesHorarias"] 
            ]
            
            transporte2 = Transporte(**objTransporte)
            db.session.add(transporte2)
            
            db.session.commit()

    @patch.object(ConsultorOrdenes, "getOrden")
    @patch.object(ConsultorLibros, "obtenerVolumenLibro")
    def testRutaProcesarOrden(self, mockObtenerVolumenLibro, mockObtenerOrden):
        with self.app.app_context():
            mockObtenerOrden.return_value = {"enviado":False,"envioId":None,"id":1,"librosCantidad":[[1,5],[1,10],[1,11]],"procesado":False}
            mockObtenerVolumenLibro.return_value = 2
            res = self.client.get('/envio/process/1')

            assert res.status_code == 200
            result = json.loads(res.data.decode())

            expected = {"boxesNeeded":{"4":3,"40":1},"volTotal":52}

            for k,v in expected.items():
                assert result[k] == v
    
    def testRutaListarEnvios(self):
        with self.app.app_context():
            envios = []
            envios += [
                {
                    "patente": "AB123CD",
                    "fecha": "18-11-2021",
                    "ordenes": [1,4,5]
                }
            ]
            envioAttr = envios[0].copy()
            envioAttr["ordenesEnvio"] = [ OrdenesPorEnvio(ordenId) for ordenId in envioAttr["ordenes"] ]
            envioAttr.pop("ordenes")
            envioObj = Envio(**envioAttr)
            db.session.add(envioObj)

            envios += [
                {
                    "patente": "EF456GH",
                    "fecha": "19-11-2021",
                    "ordenes": [1,3]
                }
            ]
            envioAttr = envios[1].copy()
            envioAttr["ordenesEnvio"] = [ OrdenesPorEnvio(ordenId) for ordenId in envioAttr["ordenes"] ]
            envioAttr.pop("ordenes")
            envioObj = Envio(**envioAttr)
            db.session.add(envioObj)
            db.session.commit()

            res = self.client.get("/envios/")

            assert res.status_code == 200

            jsonData = json.loads(res.data.decode())

            for i, jsonDataEnvio in enumerate(jsonData["envios"]):
                jsonDataEnvio.pop("id")
                for k, v in envios[i].items():
                    assert jsonDataEnvio[k] == v
    
    def testRutaObtenerEnvio(self):
        with self.app.app_context():
            envios = []
            envios += [
                {
                    "patente": "AB123CD",
                    "fecha": "18-11-2021",
                    "ordenes": [1,4,5]
                }
            ]
            envioAttr = envios[0].copy()
            envioAttr["ordenesEnvio"] = [ OrdenesPorEnvio(ordenId) for ordenId in envioAttr["ordenes"] ]
            envioAttr.pop("ordenes")
            envioObj = Envio(**envioAttr)
            db.session.add(envioObj)

            envios += [
                {
                    "patente": "EF456GH",
                    "fecha": "19-11-2021",
                    "ordenes": [1,3]
                }
            ]
            envioAttr = envios[1].copy()
            envioAttr["ordenesEnvio"] = [ OrdenesPorEnvio(ordenId) for ordenId in envioAttr["ordenes"] ]
            envioAttr.pop("ordenes")
            envioObj = Envio(**envioAttr)
            db.session.add(envioObj)
            db.session.commit()
            
            for i, envio in enumerate(envios):
                res = self.client.get(f"/envio/{i+1}")
                assert res.status_code == 200
                jsonData = json.loads(res.data.decode())
                for k, v in envio.items():
                    assert jsonData[k] == v

    def testRutaFiltrarEnvio(self):
        with self.app.app_context():
            envios = []
            envios += [
                {
                    "patente": "AB123CD",
                    "fecha": "18-11-2021",
                    "ordenes": [1,4,5]
                }
            ]
            envioAttr = envios[0].copy()
            envioAttr["ordenesEnvio"] = [ OrdenesPorEnvio(ordenId) for ordenId in envioAttr["ordenes"] ]
            envioAttr.pop("ordenes")
            envioObj = Envio(**envioAttr)
            db.session.add(envioObj)

            envios += [
                {
                    "patente": "EF456GH",
                    "fecha": "19-11-2021",
                    "ordenes": [1,3]
                }
            ]
            envioAttr = envios[1].copy()
            envioAttr["ordenesEnvio"] = [ OrdenesPorEnvio(ordenId) for ordenId in envioAttr["ordenes"] ]
            envioAttr.pop("ordenes")
            envioObj = Envio(**envioAttr)
            db.session.add(envioObj)
            db.session.commit()
            
            filters = [
                [ "AB123CD", "18-11-2021" ],
                [ "EF456GH", "19-11-2021" ]
            ]

            for i, [ patente, fecha ] in enumerate(filters):
                res = self.client.get(f"/envio/{patente}/{fecha}")
                assert res.status_code == 200
                jsonData = json.loads(res.data.decode())
                for k, v in envios[i].items():
                    assert jsonData[k] == v

    @patch.object(TransportesRepositorio, "get")
    @patch.object(EnviosRepositorio, "filter")
    @patch.object(ConsultorOrdenes, "getOrden")
    @patch.object(EnviosRepositorio, "process")
    def testRutaCrearEnvioOk(self, mockProcessEnvio, mockGetOrden, mockFilterEnvio, mockGetTransporte):
        mockGetTransporte.return_value = {
            "disponibilidadesHorarias": [
                [0,"08:00","17:00"],
                [1,"08:00","18:00"],
                [2,"08:00","18:00"],
                [3,"15:00","20:00"]
            ],
            "patente":"DE142CB",
            "volumen":30
        }
        mockFilterEnvio.side_effect = NotFoundException()
        mockGetOrden.return_value = True
        mockProcessEnvio.return_value = {"boxesNeeded":{"4":1,"40":0},"volTotal":4}

        with self.app.app_context():
            envio = {
                "patente": "AB123CD",
                "fecha": "24-11-2021",
                "ordenes": [1,4,5]
            }

            res = self.client.post("/envio/", json=envio)
            assert res.status_code == 201

            jsonData = json.loads(res.data.decode())

            id = jsonData["id"]
            envioDB = Envio.query.get(1)

            assert envioDB.ordenesEnvio[0].ordenId == envio["ordenes"][0]
            assert envioDB.ordenesEnvio[1].ordenId == envio["ordenes"][1]
            assert envioDB.ordenesEnvio[2].ordenId == envio["ordenes"][2]
            assert envioDB.patente == envio["patente"]
            assert envioDB.fecha == envio["fecha"]
    
    def testRutaCrearEnvioVehiculoInexistente(self):
        with self.app.app_context():
            envio = {
                "patente": "VEHICULONOTFOUND",
                "fecha": "24-11-2021",
                "ordenes": [1,4,5]
            }

            res = self.client.post("/envio/", json=envio)
            assert res.status_code == 400

            jsonData = json.loads(res.data.decode())
            assert jsonData["Resultado"] == "La patente del vehiculo no existe"

    @patch.object(TransportesRepositorio, "get")
    @patch.object(EnviosRepositorio, "filter")
    @patch.object(ConsultorOrdenes, "getOrden")
    @patch.object(EnviosRepositorio, "process")
    def testRutaCrearEnvioOrdenNoEncontrada(self, mockProcessEnvio, mockGetOrden, mockFilterEnvio, mockGetTransporte):
        mockGetTransporte.return_value = {
            "disponibilidadesHorarias": [
                [0,"08:00","17:00"],
                [1,"08:00","18:00"],
                [2,"08:00","18:00"],
                [3,"15:00","20:00"]
            ],
            "patente":"DE142CB",
            "volumen":30
        }
        mockFilterEnvio.side_effect = NotFoundException()
        mockGetOrden.return_value = None
        mockProcessEnvio.return_value = {"boxesNeeded":{"4":1,"40":0},"volTotal":4}

        with self.app.app_context():
            envio = {
                "patente": "AB123CD",
                "fecha": "24-11-2021",
                "ordenes": [1,4,5]
            }

            res = self.client.post("/envio/", json=envio)
            assert res.status_code == 400

            jsonData = json.loads(res.data.decode())
            assert jsonData["Resultado"] == "La orden con id 1 no existe"

    @patch.object(TransportesRepositorio, "get")
    @patch.object(EnviosRepositorio, "filter")
    @patch.object(ConsultorOrdenes, "getOrden")
    @patch.object(EnviosRepositorio, "process")
    def testRutaCrearEnvioYaExistenteEnFecha(self, mockProcessEnvio, mockGetOrden, mockFilterEnvio, mockGetTransporte):
        mockGetTransporte.return_value = {
            "disponibilidadesHorarias": [
                [0,"08:00","17:00"],
                [1,"08:00","18:00"],
                [2,"08:00","18:00"],
                [3,"15:00","20:00"]
            ],
            "patente":"DE142CB",
            "volumen":30
        }
        mockFilterEnvio.return_value = 1
        mockGetOrden.return_value = True
        mockProcessEnvio.return_value = {"boxesNeeded":{"4":1,"40":0},"volTotal":4}

        with self.app.app_context():
            envio = {
                "patente": "AB123CD",
                "fecha": "24-11-2021",
                "ordenes": [1,4,5]
            }

            res = self.client.post("/envio/", json=envio)
            assert res.status_code == 400

            jsonData = json.loads(res.data.decode())
            assert jsonData["Resultado"] == "Ya existe un envio para esa fecha para el vehiculo solicitado"

    @patch.object(TransportesRepositorio, "get")
    @patch.object(EnviosRepositorio, "filter")
    @patch.object(ConsultorOrdenes, "getOrden")
    @patch.object(EnviosRepositorio, "process")
    def testRutaCrearEnvioEnFechaNoDisponible(self, mockProcessEnvio, mockGetOrden, mockFilterEnvio, mockGetTransporte):
        mockGetTransporte.return_value = {
            "disponibilidadesHorarias": [
                [0,"08:00","17:00"],
                [1,"08:00","18:00"],
                [2,"08:00","18:00"],
                [3,"15:00","20:00"]
            ],
            "patente":"DE142CB",
            "volumen":30
        }
        mockFilterEnvio.side_effect = NotFoundException()
        mockGetOrden.return_value = True
        mockProcessEnvio.return_value = {"boxesNeeded":{"4":1,"40":0},"volTotal":4}

        with self.app.app_context():
            envio = {
                "patente": "AB123CD",
                "fecha": "26-11-2021",
                "ordenes": [1,4,5]
            }

            res = self.client.post("/envio/", json=envio)
            assert res.status_code == 400

            jsonData = json.loads(res.data.decode())
            assert jsonData["Resultado"] == "La fecha no se encuentra entre los dias disponibles para el transporte elegido"