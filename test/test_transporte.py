from . import BaseTestClass
from app import db
from entidades import Transporte, DisponibilidadTransporte
from auxiliar import ConsultorTransportes
import json

class TestTransporte(BaseTestClass):
    def testRutaListTransportes(self):
        with self.app.app_context():
            transportesData = []
            transportesData += [
                {
                    "patente": "AB123CD", 
                    "volumen": 500,
                    "disponibilidadesHorarias": [
                        [ 0, "05:00", "18:00" ],
                        [ 1, "06:00", "19:00" ],
                        [ 2, "07:00", "20:00" ]
                    ]
                }
            ]
            objTransporte = transportesData[0].copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transportesData[0].get("patente"), *disp) for disp in transportesData[0]["disponibilidadesHorarias"] 
            ]
            transporte1 = Transporte(**objTransporte)
            db.session.add(transporte1)
            
            transportesData += [
                {
                    "patente": "EF456GH", 
                    "volumen": 320,
                    "disponibilidadesHorarias": [
                        [ 0, "09:00", "18:00" ],
                        [ 1, "09:00", "18:00" ],
                        [ 2, "09:00", "17:00" ],
                        [ 3, "09:00", "16:00" ],
                        [ 4, "09:00", "15:30" ]
                    ]
                }
            ]
            objTransporte = transportesData[1].copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transportesData[1].get("patente"), *disp) for disp in transportesData[1]["disponibilidadesHorarias"] 
            ]
            
            transporte2 = Transporte(**objTransporte)
            db.session.add(transporte2)
            
            db.session.commit()
            
            res = self.client.get('/transportes/')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            for i, transporte in enumerate(transportesData):
                for k,v in transporte.items():
                    assert jsonData["transportes"][i][k] == v

    def testRutaCrearTransporte(self):
        with self.app.app_context():
            transporte = {
                "patente": "AB123CD", 
                "volumen": 500,
                "disponibilidadesHorarias": [
                    [ 0, "05:00", "18:00" ],
                    [ 1, "06:00", "19:00" ],
                    [ 2, "07:00", "20:00" ]
                ]
            }
            
            res = self.client.post('/transporte/', json=transporte)
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 201
            for k,v in transporte.items():
                assert transporte[k] == v

    def testRutaCrearTransporteFechaMal(self):
        with self.app.app_context():
            transporte = {
                "patente": "AB123CD", 
                "volumen": 500,
                "disponibilidadesHorarias": [
                    [ -1, "05:00", "18:00" ],
                    [ 1, "06:00", "19:00" ],
                    [ 2, "07:00", "20:00" ]
                ]
            }
            
            res = self.client.post('/transporte/', json=transporte)
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 400
            assert jsonData["Resultado"] == "El dia semanal -1 debe estar entre los valores 0..6"

    def testRutaCrearTransporteHorarioMal(self):
        with self.app.app_context():
            transporte = {
                "patente": "AB123CD", 
                "volumen": 500,
                "disponibilidadesHorarias": [
                    [ 0, "HORA MAL", "18:00" ],
                    [ 1, "06.00", "19:00" ],
                    [ 2, "07:00", "20:00" ]
                ]
            }
            
            res = self.client.post('/transporte/', json=transporte)
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 400
            assert jsonData["Resultado"] == "time data 'HORA MAL' does not match format '%H:%M'"

    def testRutaObtenerTransporte(self):
        with self.app.app_context():
            transportesData = []
            transportesData += [
                {
                    "patente": "AB123CD", 
                    "volumen": 500,
                    "disponibilidadesHorarias": [
                        [ 0, "05:00", "18:00" ],
                        [ 1, "06:00", "19:00" ],
                        [ 2, "07:00", "20:00" ]
                    ]
                }
            ]
            objTransporte = transportesData[0].copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transportesData[0].get("patente"), *disp) for disp in transportesData[0]["disponibilidadesHorarias"] 
            ]
            transporte1 = Transporte(**objTransporte)
            db.session.add(transporte1)
            
            transportesData += [
                {
                    "patente": "EF456GH", 
                    "volumen": 320,
                    "disponibilidadesHorarias": [
                        [ 0, "09:00", "18:00" ],
                        [ 1, "09:00", "18:00" ],
                        [ 2, "09:00", "17:00" ],
                        [ 3, "09:00", "16:00" ],
                        [ 4, "09:00", "15:30" ]
                    ]
                }
            ]
            objTransporte = transportesData[1].copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transportesData[1].get("patente"), *disp) for disp in transportesData[1]["disponibilidadesHorarias"] 
            ]
            
            transporte2 = Transporte(**objTransporte)
            db.session.add(transporte2)
            
            db.session.commit()
            
            
            for i, transporte in enumerate(transportesData):
                res = self.client.get(f'/transporte/{transporte["patente"]}')
                assert res.status_code == 200

                jsonData = json.loads(res.data.decode())    
                
                for k,v in transporte.items():
                    assert jsonData[k] == v

    def testRutaActualizarTransporte(self):
        with self.app.app_context():
            transporteBefore = {
                "patente": "AB123CD", 
                "volumen": 500,
                "disponibilidadesHorarias": [
                    [ 0, "05:00", "18:00" ],
                    [ 1, "06:00", "19:00" ],
                    [ 2, "07:00", "20:00" ]
                ]
            }
            objTransporte = transporteBefore.copy()
            objTransporte["disponibilidadesHorarias"] = [ 
                DisponibilidadTransporte(transporteBefore.get("patente"), *disp) for disp in transporteBefore["disponibilidadesHorarias"] 
            ]
            transporte1 = Transporte(**objTransporte)
            db.session.add(transporte1)
            
            transporteAfter = {
                "patente": "AB123CD", 
                "volumen": 320,
                "disponibilidadesHorarias": [
                    [ 0, "05:00", "18:00" ],
                    [ 1, "06:00", "19:00" ],
                    [ 2, "07:00", "20:00" ]
                ]
            }
            
            res = self.client.put(f'/transporte/{transporteBefore["patente"]}', json=transporteAfter)
            assert res.status_code == 200

            jsonData = json.loads(res.data.decode())    
            assert jsonData["patente"] == transporteAfter["patente"]

            res = self.client.get(f'/transporte/{transporteBefore["patente"]}')                
            assert res.status_code == 200
            jsonData = json.loads(res.data.decode())                    
            
            for k,v in transporteAfter.items():
                assert jsonData[k] == v

    def testConsultorFechaPorDia(self):
        assert ConsultorTransportes.isPastDate("15-11-2021")