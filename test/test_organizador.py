from . import BaseTestClass
from auxiliar import BinPacking

class TestOrganizador(BaseTestClass):
    CAPACITIES = [40, 4]

    def testOrganizar_2de4(self):
        r = BinPacking.packAll(self.CAPACITIES, [3,3])
        assert r[4] == 2
        assert r[40] == 0

    def testOrganizar_1de40(self):
        r = BinPacking.packAll(self.CAPACITIES, [4,4,4,4,4,4,4,4,4,4])
        assert r[4] == 0
        assert r[40] == 1

    def testOrganizar_1de40_2de4(self):
        r = BinPacking.packAll(self.CAPACITIES, [4,4,4,4,4,4,4,4,4,4,3,3])
        assert r[4] == 2
        assert r[40] == 1