from os import environ

class Config:
    SQLALCHEMY_DATABASE_URI = 'sqlite:////envios/db/sqlite.db'
    FLASK_ENV = environ.get("FLASK_ENV")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
class ConfigTest(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///db/sqlite_test.db'
    TESTING = True
    DEBUG = True
