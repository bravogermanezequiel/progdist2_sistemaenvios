FROM python:3.9
WORKDIR /envios
COPY . /envios/
RUN pip install -r requirements.txt