# ¿Que tests se realizaron?

Se llevaron a cabo tests sobre todas las entidades y operaciones referidas a verbos API de forma unitaria. Para todo se verifica la salida y codigo de respuesta esperados.

## Envio
**Se establece un setUp de dos transportes con diferentes patentes y disponibilidades**

- Procesar orden valida: se envia a procesar un envio con orden {id} a la API GET /envio/process/1, y se valida que el resultado de cajas necesarias y volumen total ocupado sea el esperado. Se mockeo la función getOrden (para que devuelva una orden de acuerdo a nuestras necesidades) y obtenerVolumenLibro (para que siempre devuelve que cualquier libro tiene un volumen igual a 2 por unidad).
- Listar envios: se realiza la carga de dos envios por medio del ORM y se obtienen por medio de la API GET /envios/, para luego validar sus atributos.
- Obtener envio valido: se realiza la carga de dos envios por medio del ORM y se obtienen por medio de la API GET /envio/{id} uno a uno, para luego validar sus atributos.
- Filtrar envio valido: se realiza la carga de dos envios por medio del ORM y se obtienen por medio del filtro de API GET /envio/{patente}{fecha} uno a uno, para luego validar sus atributos y si son correctos.
- Crear envio valido: se realiza la creacion de un envio por medio de la API POST /envio/ y luego se valida contra el ORM. Se debio mockear las funciones TransportesRepositorio.get (para obtener un transporte), EnviosRepositorio.filter (para que lance una excepcion de no encontrado), ConsultorOrdenes.getOrden (para que devuelva verdadero siempre lo que implica la existencia de la orden), y EnviosRepositorio.process (para que devuelva un numero de cajas y volumen total estatico).
- Crear envio con vehiculo inexistente: se crea un envio por medio de la API POST /envio/ con una patente de vehiculo inexistente y se valida su no creación y los mensaje de retorno de la API.
- Crear envio con orden inexistente: se crea un envio por medio de la API POST /envio/ con un id de orden inexistente y se valida su no creación y los mensaje de retorno de la API.
- Crear envio para el mismo vehiculo en la misma fecha: se crea un envio mockeando la funcion EnviosRepositorio.filter para que devuelve un 1 siempre, lo que indicaria la ya existencia de un envio para tal patente y fecha, para lo cual luego se valida que la API devuelva el codigo y mensajes correctos.
- Crear envio para un vehiculo en una fecha no disponible: se crea un envio para un transporte con tal patente, en una fecha que no entre dentro de sus disponibilidades semanales y se validan los mensajes de retorno.

## Transporte:

- Listar transportes: se cargan dos transportes por medio de ORM y se obtiene por medio de la API GET /transportes/, para luego validar atributos y mensajes.
- Crear transporte valido: se crea transporte con datos validos por medio de la API POST /transporte, y luego se valida contra el ORM tal transporte y sus atributos.
- Crear transporte con fecha mal: se crea un transporte con una disponibilidad para el dia -1, el cual no es valido y se validan los mensajes de retorno y codigos.
- Crear transporte con horario mal: se crea un transporte con una disponibilidad para un horario que no respeta el formato del TIME, lo cual no es valida y se validan los mensajes de retorno y codigos.
- Obtener transporte valido: se cargan dos transportes por medio de ORM y se obtiene por medio de la API GET /transporte/{id} uno a uno, para luego validar atributos y mensajes.
- Actualizar transporte: se carga un transporte por medio de ORM y luego por medio de la API PUT /transporte/{id} se actualiza con datos nuevos de tal manera que luego se validan atributos y mensajes de retorno.

## Misc - Organizador

- Se valida en varias pruebas la clase estatica BinPacking utilizada como algoritmo para organizar pedido de libros en cajas de dos tamaños (4 y 40).
