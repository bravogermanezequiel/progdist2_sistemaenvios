# API Envios

## Entidades

- Transporte
    1. patente
    2. volumen
    3. disponibilidadesHorarias
- DisponibilidadTransporte
    1. id
    2. patente
    3. diaSemanal
    4. horarioInicial
    5. horarioFinal
- Envio
    1. id
    2. patente
    3. fecha
    4. ordenesEnvio
- OrdenesPorEnvio
    1. id
    2. envioId
    3. ordenId

## Rutas

- Transporte
    1. Listas transportes:  GET /transportes/
    2. Obtener transporte:  GET /transporte/{string:patente}
    3. Crear transporte:    POST /transporte/
    4. Actualizar transporte:   PUT /transporte/{string:patente}
    5. Listar transportes con fechas de disponibilidad por semana relativa: GET /transportes/dates/{int:week}
- Envio
    1. Listas envios:  GET /envios/
    2. Obtener envio:  GET /envio/{int:id}
    3. Crear envio:    POST /envio/
    4. Actualizar envio:   PUT /envio/{int:id}
    5. Calcular volumen de envio (devuelve cantidad de cajas y volumen total necesario): GET /envio/process/{int:ordenId}
    6. Filtrar envios por patente y fecha: GET /envio/{string:patente}/{string:fecha}
